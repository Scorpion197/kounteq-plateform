import React, { useState } from "react";
import { useParams } from "react-router-dom";
import millify from "millify";
import { Col, Row, Typography, Select } from "antd";
import { DollarCircleOutlined } from "@ant-design/icons";

import { useCryptoDetailsFetch } from "../../Hooks/useCryptoDetailsFetch";

import Loader from "../Loader/Loader";
import Navbar from "../Navbar/Navbar";
import LineChart from "../LineChart/LineChart";

import "./details.css";

import API from "../../API";
const { Title, Text } = Typography;
const { Option } = Select;

const CryptoDetails = () => {
  const { coinId } = useParams();
  const { loading, error, state } = useCryptoDetailsFetch(coinId);
  const coinData = state;
  const [timePeriod, setTimePeriod] = useState("7d");
  const [coinHistory, setCoinHistory] = useState([]);
  const [historyLoading, setHistoryLoading] = useState(true);

  const fetchCoinHistory = async () => {
    try {
      const data = await API.fetchCoinHistory(coinId);
      if (data.hasOwnProperty("historyitems")) {
        setCoinHistory(data?.historyitems);

        setHistoryLoading(false);
      }
    } catch (error) {
      console.log("error while fetching coin history: ", error);
    }
  };
  const time = ["24h"];

  const stats = [
    {
      title: "Market Cap",
      value: `$ ${coinData?.market_cap && millify(coinData?.market_cap)}`,
      icon: <DollarCircleOutlined />,
    },
  ];
  useState(() => {
    fetchCoinHistory();
  }, []);

  if (loading || coinHistory?.length <= 0 || historyLoading) {
    return <Loader />;
  } else
    return (
      <>
        <Navbar />
        <div className="coin-detail-container">
          <div className="coin-heading-container">
            <Title level={2} className="coin-name">
              {coinData.name} ({coinData.symbol}) Price
            </Title>
            <p>
              {coinData.name} live price in US Dollar (USD). View value
              statistics, market cap and supply.
            </p>
          </div>

          <Select
            defaultValue="24h"
            className="select-timeperiod"
            placeholder="Select Time Period"
            onChange={(value) => setTimePeriod(value)}
          >
            {time.map((date) => (
              <Option key={date}>{date} </Option>
            ))}
          </Select>
          <div className="line-chart">
            <LineChart
              coinHistory={coinHistory}
              currentPrice={coinHistory[0]?.price}
              coinName={coinData.name}
            />
          </div>

          <Col className="stats-container">
            <Col className="coin-value-statistics">
              <Col className="coin-value-statistics-heading">
                <Title level={3} className="coin-details-heading">
                  {coinData.name} Value Stats
                </Title>
                <p>An overview showing the stats of {coinData.name}</p>
              </Col>
              {stats.map(({ icon, title, value }) => (
                <Col className="coin-stats">
                  <Col className="coin-stats-name">
                    <Text> {icon} </Text>
                    <Text> {title} </Text>
                  </Col>
                  <Text className="stats">{value}</Text>
                </Col>
              ))}
            </Col>
            <Col className="other-stats-info">
              <Col className="coin-value-statistics-heading">
                <Title level={3} className="coin-details-heading">
                  More Statistics
                </Title>
                <p>An overview showing the stats of all cryptocurrencies</p>
              </Col>
            </Col>
          </Col>
        </div>
      </>
    );
};

export default CryptoDetails;
