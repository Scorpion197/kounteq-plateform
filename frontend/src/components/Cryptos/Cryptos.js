import React, { useState } from "react";
import { Link } from "react-router-dom";
import millify from "millify";
//antd
import { Card, Row, Col, Input } from "antd";

//hooks
import { useCurrenciesFetch } from "../../Hooks/useCurrenciesFetch";
//css
import "./cryptos.css";

//components
import Loader from "../Loader/Loader";

const Cryptos = ({ simplified }) => {
  const count = simplified ? 10 : 50;
  const { curLoading, curError, curState } = useCurrenciesFetch(count);
  var topCurrencies = [];

  if (curState.length != 0 && simplified) topCurrencies = curState.slice(0, 10);
  else topCurrencies = curState;

  if (curLoading) return <Loader />;

  return (
    <div>
      <Row gutter={[32, 32]} className="crypto-card-container">
        {topCurrencies.map((currency) => (
          <Col
            xs={24}
            sm={12}
            lg={6}
            className="crypto-card"
            key={currency.uuid}
          >
            <Link to={`/crypto/${currency.id}`}>
              <Card
                title={`${currency.symbol}. ${currency.name}`}
                extra={<img className="crypto-image" src={currency.icon_url} />}
                hoverable
              >
                <p> Market Cap: {millify(currency.market_cap)}</p>
              </Card>
            </Link>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default Cryptos;
