import React from "react";
import millify from "millify";
import { Typography, Row, Col, Statistic, Divider } from "antd";
import { Link } from "react-router-dom";

//components
import Cryptos from "../Cryptos/Cryptos";
import Navbar from "../Navbar/Navbar";
import Loader from "../Loader/Loader";

import "./home.css";

//hooks
import { useCurrenciesFetch } from "../../Hooks/useCurrenciesFetch";

const { Title } = Typography;

const HomePage = () => {
  const { loading, error, state } = useCurrenciesFetch();

  if (loading) return <Loader />;

  return (
    <div className="main-container">
      <div className="navbar-container">
        <Navbar className="navbar" />
      </div>
      <div className="grid-container">
        <div className="sub-grid-container">
          <Title level={2} className="heading">
            Global Crypto Stats
          </Title>

          <Row>
            <Col span={12}>
              <Statistic
                className="stat-title"
                title="Total Cryptocurrencies"
                value={millify(1325469.2364)}
              />
            </Col>
            <Col span={12}>
              <Statistic
                className="stat-title"
                title="Total Exchanges"
                value={2688921.69}
              />
            </Col>
            <Col span={12}>
              <Statistic
                className="stat-title"
                title="Total Coins"
                value={millify(9635487.58)}
              />
            </Col>
            <Col span={12}>
              <Statistic
                className="stat-title"
                title="Total Market Cap"
                value={`$ ${millify(9635487.58)}`}
              />
            </Col>
            <Col span={12}>
              <Statistic
                className="stat-title"
                title="Total 24h volume"
                value={`$ ${millify(56987.87)}`}
              />
            </Col>
            <Col span={12}>
              <Statistic
                className="stat-title"
                title="Total Markets"
                value={millify(698223598.25)}
              />
            </Col>
          </Row>

          <div className="home-heading-container">
            <Title level={2} className="home-title">
              {" "}
              Top 10 Cryptos In The World
            </Title>
            <Title level={3} className="show-more">
              <Link className="show-more-link" to="/cryptos">
                Show more
              </Link>
            </Title>
          </div>

          <Cryptos simplified={true} />
        </div>
      </div>
    </div>
  );
};

export default HomePage;
