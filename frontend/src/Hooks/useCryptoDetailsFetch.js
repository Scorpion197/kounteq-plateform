import React, { useState, useEffect } from "react";

import API from "../API";

export const useCryptoDetailsFetch = (cryptoId) => {
  const initialState = {};
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [state, setState] = useState(initialState);

  const getData = async (cryptoId) => {
    try {
      setLoading(true);
      setError(false);

      const data = await API.fetchCryptoDetails(cryptoId);

      setLoading(false);
      setState(data);
    } catch (error) {
      setError(true);
    }
  };

  useEffect(() => {
    getData(cryptoId);
  }, []);

  return { loading, error, state };
};
