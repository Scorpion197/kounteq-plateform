# About 
This a recruitement assessment for Django Developer Position at Kounteq. 
## Plateform 
This platform displays all the cryptocurrencies available in the market as will as their price history 
## Overview
 - Home page to display all the cryptocurrencies 
    ![home page](./assets/kounteq1.png)

 - History of a cryptocurrency (bitcoin in this screenshot)
    ![bitcoin history](./assets/kounte3.png)

## API documentation 
The documentation of the api has been created with swagger and can be found at:
`http://localhost:8000/api/swagger/`

## Tests 
Unit tests has been created and can be found inside `tests` folder in the application folder. 

## Running the project 
 - clone the project and run `docker-compose build` and then `docker-compose up`. 
 - you can access the application at `http://localhost:3000`