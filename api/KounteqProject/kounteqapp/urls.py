from django.urls import path, re_path
from rest_framework import routers
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from .views import *

schema_view = get_schema_view(
    openapi.Info(
        title="Backend API",
        default_version="v1",
        description="Kounteq Finance Application",
        contact=openapi.Contact("kamelgaouaoui197@gmail.com"),
        license=openapi.License(name="BSD license"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)
router = routers.DefaultRouter()

router.register("cryptocurrencies", CryptocurrencyViewset, "cryptocurrencies")

urlpatterns = [
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path(
        "cryptocurrencies/history/<int:pk>",
        CryptocurrencyHistoryViewset.as_view(),
        name="cryptocurrency_history",
    ),
]

urlpatterns += router.urls
