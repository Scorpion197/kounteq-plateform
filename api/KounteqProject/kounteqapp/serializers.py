from rest_framework import serializers
from .models import *


class CryptocurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Cryptocurrency
        fields = "__all__"


class HistoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = HistoryItem
        fields = "__all__"


class CryptocurrencyHistorySerializer(serializers.ModelSerializer):
    historyitems = HistoryItemSerializer(many=True, read_only=True)

    class Meta:
        model = Cryptocurrency
        fields = ["id", "name", "historyitems"]
