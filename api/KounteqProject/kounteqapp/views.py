from rest_framework import viewsets, generics
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from django.shortcuts import get_object_or_404

from .models import *
from .serializers import *


class CryptocurrencyViewset(viewsets.ViewSet):
    permission_classes = [AllowAny]

    def list(self, request):
        queryset = Cryptocurrency.objects.all()
        serializer = CryptocurrencySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Cryptocurrency.objects.all()
        cryptocurrency = get_object_or_404(queryset, pk=pk)
        serializer = CryptocurrencySerializer(cryptocurrency, many=False)
        return Response(serializer.data)


class CryptocurrencyHistoryViewset(generics.RetrieveAPIView):
    permission_classes = [AllowAny]
    queryset = Cryptocurrency.objects.all()
    serializer_class = CryptocurrencyHistorySerializer
