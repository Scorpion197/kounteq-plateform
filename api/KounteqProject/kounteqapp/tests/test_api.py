from django.test import TestCase, RequestFactory

from rest_framework import status

from kounteqapp.models import *
from kounteqapp.views import *


class CryptocurrencyViewsetTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.bictoin_instance = Cryptocurrency.objects.first()

    def test_get_cryptocurrencies(self):
        request = self.factory.get("/api/views/cryptocurrencies")
        response = CryptocurrencyViewset.as_view({"get": "list"})(request)

        self.assertEqual(
            len(response.data), 50
        )  # there are 50 cryptocurrencies in the database
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_one_cryptocurrency(self):
        request = self.factory.get("/api/views/cryptocurrencies/1")
        response = CryptocurrencyViewset.as_view({"get": "retrieve"})(
            request, pk=self.bictoin_instance.id
        )
        self.assertEqual(response.data["name"], self.bictoin_instance.name)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class HistoryItemViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.bitcoin_instance = Cryptocurrency.objects.first()
        self.bitcoin_history = HistoryItem.objects.filter(crypto=self.bitcoin_instance)

    def test_bitcoin_history(self):
        request = self.factory.get(
            f"/api/views/cryptocurrencies/history/{self.bitcoin_instance.id}"
        )
        response = CryptocurrencyHistoryViewset.as_view()(
            request, pk=self.bitcoin_instance.id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data["historyitems"]), self.bitcoin_history.count()
        )
