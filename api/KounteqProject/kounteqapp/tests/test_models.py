from django.test import TestCase
from kounteqapp.models import *


class ModelsTest(TestCase):
    def setUp(self) -> None:
        return super().setUp()

    def test_cryptocurrency(self):
        # after inspecting the data the first cryptocurrency is Bitcoin
        bitcoin_instance = Cryptocurrency.objects.first()
        self.assertEqual(bitcoin_instance.name, "Bitcoin")

    def test_itemhistory(self):
        bitcoin_instance = Cryptocurrency.objects.first()
        history_item_instance = HistoryItem.objects.first()

        self.assertEqual(bitcoin_instance.id, history_item_instance.crypto.id)
