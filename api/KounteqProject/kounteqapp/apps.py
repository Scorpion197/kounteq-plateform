from django.apps import AppConfig


class KounteqappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kounteqapp'
