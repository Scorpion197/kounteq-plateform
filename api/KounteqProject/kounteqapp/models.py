from django.db import models


class Cryptocurrency(models.Model):
    name = models.CharField(max_length=50, default="")
    symbol = models.CharField(max_length=20, default="")
    icon_url = models.CharField(max_length=100, default="")
    uuid = models.CharField(max_length=50, default="")
    market_cap = models.PositiveBigIntegerField()

    def __str__(self) -> str:
        return self.name


class HistoryItem(models.Model):
    price = models.PositiveIntegerField()
    timestamp = (
        models.PositiveIntegerField()
    )  # timestamp will be something like: 1625764100
    crypto = models.ForeignKey(
        Cryptocurrency,
        related_name="historyitems",
        on_delete=models.CASCADE,
        null=False,
    )
