import requests


class ApiWrapper:
    def __init__(self, api_key, api_url):
        self.api_key = api_key
        self.api_url = api_url

    def get_all_cryptos(self):
        url = f"{self.api_url}/coins"
        params = {
            "referenceCurrencyUuid": "yhjMzLPhuIDl",
            "timePeriod": "24h",
            "tiers[0]": "1",
            "orderBy": "marketCap",
            "orderDirection": "desc",
            "limit": "50",
            "offset": "0",
        }

        headers = {
            "X-RapidAPI-Key": self.api_key,
            "X-RapidAPI-Host": "coinranking1.p.rapidapi.com",
        }

        response = requests.get(url, params=params, headers=headers).json()["data"][
            "coins"
        ]

        parsed_data = [
            {
                "name": item["name"],
                "symbol": item["symbol"],
                "icon_url": item["iconUrl"],
                "uuid": item["uuid"],
                "market_cap": item["marketCap"],
            }
            for item in response
        ]

        return parsed_data

    def get_crypto_price_history(self, crypto_uuid):
        url = f"{self.api_url}/coin/{crypto_uuid}/history"
        params = {"referenceCurrencyUuid": "yhjMzLPhuIDl", "timePeriod": "24h"}
        headers = {
            "X-RapidAPI-Key": self.api_key,
            "X-RapidAPI-Host": "coinranking1.p.rapidapi.com",
        }

        response = requests.get(url, params=params, headers=headers).json()
        return response["data"]["history"]
